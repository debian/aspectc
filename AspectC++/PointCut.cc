// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#include "PointCut.h"

PointCut::PointCut() {
  _type = PCT_UNKNOWN;
//  _contents = (pccontents)0;
}

PointCut::~PointCut () {
  for (list<PointCut*>::iterator iter = _cflow_triggers.begin ();
       iter != _cflow_triggers.end (); ++iter)
    delete *iter;
}

void PointCut::cflow_triggers(PointCut &pc) {
  PointCut *triggers = new PointCut();
  *triggers = pc;
  _cflow_triggers.push_back (triggers);
}

const list<PointCut*> &PointCut::cflow_triggers() {
  return _cflow_triggers;
}

PointCutMapType::iterator PointCut::insert (ACM_Any *jpl, Condition &cond) {
  auto result = PointCutMapType::insert(PointCutMapType::value_type(jpl, Condition()));
  assert(result.second == true);
  result.first->second.assign(cond);
  return result.first;
}

ostream& operator<< (ostream& o, PointCut& p) {
  for (auto jp : p) {
    o << (void *)jp.first << ' ';
    o << jp.second << endl;
  }
  return o;
}
