// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//
// This program is free software;  you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA

#ifndef __ClangBinding_h__
#define __ClangBinding_h__

// This module is needed to hook into the internal behavior of the Clang C++
// parser. Calls to a number of Sema::ActOn... functions are wrapped by means
// of the linker option --wrap=<symbol>. This method has a number restrictions:
//  - only works with gnu ld
//  - it does not work on MacOS X
//  - it only works if calls cannot be resolved by the compiler (an undefined
//    symbol references is needed by the linker)
// All that wouldn't be need if the ActOn...-functions of clang::Sema were
// virtual. We hope that this will be changed in the future.
// The implementation hooks into calls from clang::Parser to clang::Sema::ActOn*,
// cast the Sema-object reference to a ClangIntroSema-reference and calls the
// respective ActOn...-function. For ClangIntroSema this looks like the execution
// of a normal virtual function.
// In order to call the original ActOn-function, ClangIntroSema functions call
// Real..., which is provided by this module for all wrapped functions. Calling
// the respective base class method is not possible here, because due to the
// wrapping we would end up with an infinite loop.

namespace clang {
  class Sema;
  class Scope;
  class Decl;
}

#include "clang/Basic/SourceLocation.h"
#include "version.h"

bool RealcanSkipFunctionBody(clang::Sema &that, clang::Decl *D);

#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_14_0_0
void RealProcessStmtAttributes(clang::Sema &that, clang::Stmt *S, const clang::ParsedAttributes &InAttrs, clang::SmallVectorImpl<const clang::Attr *> &OutAttrs);
#else
clang::StmtResult RealProcessStmtAttributes(clang::Sema &that, clang::Stmt *S, const clang::ParsedAttributesView &AttrList, clang::SourceRange Range);
#endif

extern ClangIntroSema *intro_sema;

#endif // __ClangBinding_h__
