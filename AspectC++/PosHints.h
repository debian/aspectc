// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#ifndef __PosHints_h__
#define __PosHints_h__

#include <list>
#include "ACToken.h"

#ifdef FRONTEND_CLANG
#include <clang/Basic/SourceLocation.h>
#endif // FRONTEND_CLANG

// FIXME: Current implementation only support FRONTEND_CLANG
class PosHint {
#ifdef FRONTEND_CLANG
  clang::SourceLocation _loc;
  int _macro_pos;
public:
  PosHint (clang::SourceLocation loc, int macro_pos) : _loc(loc), _macro_pos(macro_pos) {}
  const clang::SourceLocation &loc () const { return _loc; }
  int macro_pos () const { return _macro_pos; }
#endif // FRONTEND_CLANG
};

typedef std::list<PosHint> PosHints;

#endif // __PosHints_h__
