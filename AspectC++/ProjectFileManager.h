// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#ifndef __ProjectFileManager_h__
#define __ProjectFileManager_h__

// The PathManager class stores a list of directories that contain
// the source code files of the currently compiled AspectC++ project.
// The manager can check whether a given file belongs to the project,
// i.e. is a file store in one of the project directories or their
// sub-directories.
// For each directory path a corresponding destination path may be
// provided.

#include "Puma/PathManager.h" // TODO: avoid Puma-dependency

class ProjectFileManager {

  mutable Puma::ErrorStream _err;
  Puma::PathManager _manager;

public:
  ProjectFileManager() : _manager(_err) {}

  void addPath(const char *source, const char *destination); // ACProject (configure...)
  void protect(const char *path) { _manager.protect(path); } // ACProject (in configure...)

  bool isBelow(const char *file) { return _manager.isBelow(file); } // ClangFileTracker, ClangIncludeExpander, ClangSyntacticContext, ClangIntroSema. ClangModelBuilder, ClangIntroducer, IncludeGraph, ClangPreprocessor, ClangASTConsumer, Weaver
  long numPaths() const { return _manager.numPaths(); } // ACConfig, ClangModelBuilder, ACProject, Weaver
  const char *src(long n) const; // ACConfig, ClangModelBuilder
  const char *dest(long n) const; // ACConfig, ACProject, Weaver
  typedef Puma::PathIterator PathIterator;
  bool iterate(PathIterator &iterator) const { return _manager.iterate(iterator); } // Weaver, Transformer
  bool getDestinationPath(const char *sourcePath, std::ostream &out) const { return _manager.getDestinationPath(sourcePath, out); } // Weaver
  bool isNewer (const char *file) const; // Weaver
  void addFile (const char *src, const char *dest) { _manager.addFile(src, dest); } // ACProject
  void save (const char *name, bool is_modified, const char *content) const; // ACProject
private:
  typedef Puma::ProjectFile::MapConstIter MapConstIter;
  bool isBelow(const char *name, MapConstIter &iter) const { return _manager.isBelow(name, iter); }
  bool make_dir_hierarchy (Puma::Filename path) const;
  bool make_dir (const char *directory) const;
};

#endif // __ProjectFileManager_h__
