// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//
// This program is free software;  you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA

#ifndef __ClangIntroSema_h__
#define __ClangIntroSema_h__

#include "clang/Sema/Sema.h"
#include "version.h"

class ClangIntroducer;

class ClangIntroSema {
  clang::Sema &_sema;
  ClangIntroducer *_introducer;

public:
  ClangIntroSema(clang::Sema &sema, ClangIntroducer &introducer);

  bool canSkipFunctionBody(clang::Decl *FctDecl);

#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_14_0_0
  void ProcessStmtAttributes(clang::Stmt *S, const clang::ParsedAttributes &InAttrs, clang::SmallVectorImpl<const clang::Attr *> &OutAttrs);
#else
  clang::StmtResult ProcessStmtAttributes(clang::Stmt *S, const clang::ParsedAttributesView &AttrList, clang::SourceRange Range);
#endif
};

#endif // __ClangIntroSema_h__
