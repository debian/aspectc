// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//
// This program is free software;  you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA

#ifndef __ClangFileTracker_h__
#define __ClangFileTracker_h__

// With the help of the preprocessor we track whether we are either
// in a file that belongs to the project or in an external file.

#include "clang/Lex/PPCallbacks.h"
#include "ACConfig.h"

class ClangFileTracker : public clang::PPCallbacks {
  ACConfig &_config;
  bool _in_project;
  bool _ac_keywords_enabled;
public:
  ClangFileTracker(ACConfig &config);
  virtual void FileChanged (clang::SourceLocation Loc, FileChangeReason Reason,
      clang::SrcMgr::CharacteristicKind FileType, clang::FileID PrevFID) override;
  bool in_project() const { return _in_project; }
  bool ac_keywords_enabled() const { return _ac_keywords_enabled; }
};

#endif /* __ClangFileTracker_h__ */
