
// FIXME: This is a hack to give ClangIntroParser access rights to a private function
// This has to be included FIRST!
#define private protected
#include "clang/Parse/Parser.h"
#undef private

#include "ClangIntroParser.h"
using namespace clang;

void ClangIntroParser::ParseBaseClause(Decl *TagDecl) {
  Parser::ParseBaseClause(TagDecl);
}
