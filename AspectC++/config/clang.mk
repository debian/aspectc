# set makefile variables according to the installed Clang version

# check whether llvm-config is available
ifeq ($(shell which $(LLVMCONF)),)
  $(error FRONTEND is 'Clang', but LLVMCONF ($(LLVMCONF)) is not found)
endif

LLVM_VERSION := $(shell $(LLVMCONF) --version)

# check if the config file for this version exists
ifeq ($(wildcard config/clang-$(LLVM_VERSION).mk),)
  $(error UNSUPPORTED Clang version '$(LLVM_VERSION)' used -- try 'make -C config clang LLVMCONF=$(LLVMCONF)')
endif

include config/clang-$(LLVM_VERSION).mk

ifeq ($(PLATFORM),macos) # on macOS the symbols have an additional '_'
  LLVM_SYMBOLS      := $(patsubst %, _%, $(LLVM_SYMBOLS))
  LLVM_SYMBOLS_SEMA := $(LLVM_SYMBOLS)
endif
