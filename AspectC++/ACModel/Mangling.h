// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#ifndef __Mangling_h__
#define __Mangling_h__

#include <iostream>

// This namespace can generate a mangled string representation for
// named model elements

class ACM_Function;
class ACM_Name;
class ACM_Type;
class ACM_Arg;

namespace Mangling {

  bool is_unary_op (ACM_Function *func);
  void scope_name (std::ostream &out, ACM_Name &scope);
  void op_name (std::ostream &out, ACM_Function *func);
  void constr_name (std::ostream &out, ACM_Function *func);
  void destr_name (std::ostream &out, ACM_Function *func);
  void mangle (std::ostream &out, ACM_Name *obj);
  void mangle (std::ostream &out, ACM_Type *type);
  void mangle (std::ostream &out, const ACM_Arg *arg);

}

#endif // __Mangling_h__
