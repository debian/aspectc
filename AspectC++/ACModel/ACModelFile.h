// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//
// This program is free software;  you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA  02111-1307  USA

#ifndef __ACModelFile_h__
#define __ACModelFile_h__
#include <string>

class XmlModelReader;
class XmlModelWriter;

// Filesystem operations related to model files

class ACModelFile {
  std::string filename_;
  int fd_;
  // Lock the open file: only for internal use
  bool lock();
  // Get the file descriptor: only for XmlModelReader/Writer
  int get_fd() const { return fd_; }
  // Get the stored filename: only for XmlModelReader/Writer
  const std::string &get_filename() const { return filename_; }
  friend class XmlModelReader;
  friend class XmlModelWriter;
public:
  ACModelFile(const std::string &filename);
  // Create and lock the repository file if it does not exist.
  // If it exists, open it for writing, get an exclusive lock and
  // truncate it.
  bool create(int mode = 0600);
  // Open and lock the file for exclusive read/write access
  bool open();
  // Unlock and close the file
  bool close();
};

#endif // __ACModelFile_h__
