// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2003  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#include "AdviceInfo.h"
#include "AspectInfo.h"
#include "Binding.h"
#include "TransformInfo.h"

AdviceInfo::AdviceInfo (AspectInfo &ai, ACM_AdviceCode &c) :
  _aspect_info (ai), _aspect (ai.loc ()), _code (c) {}

std::string AdviceInfo::name () const { return TI_AdviceCode::of (_code)->name (); }

void AdviceInfo::addTJPFlags(ThisJoinPoint &tjp) {
  tjp.merge (TI_AdviceCode::of (code ())->this_join_point ());
  tjp.merge (aspect_info().aspectof_this_join_point());

  ThisJoinPoint context_vars;
  context_vars.setup (binding ());
  tjp.merge (context_vars);
}

