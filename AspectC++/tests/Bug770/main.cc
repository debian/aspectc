// This test tests the phase 1 de-anonymization of unions.
// If it compiles it is fine.

static union {
  int i;
};

union {
  int i_b;
} name1;


namespace {
  static union {
    int i2;
  };
}

namespace {
  union {
    int i2_b;
  } name2;
}

namespace {
  static union {
    int i2_c;
  } name2_c;
}


namespace N {
  static union {
    int i3;
  };
}

namespace N {
  union {
    int i3_b;
  } name3;
}

namespace N {
  static union {
    int i3_c;
  } name3_c;
}

class {
  union {
    int i4;
  };
} c;

class C {
  union {
    int i4;
  };
};

class C_b {
  static union {
    int i4;
  } name4;
};

class C_c {
  union {
    int i4;
  } name4;
};


void f() {
  static union {
    int i4;
  };
}

int main() { }
