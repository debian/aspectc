//=========================================================
// main.cpp
//
// Program main function that demonstrates the Observer
// Design pattern.
//
// In this program:
//
// ConcreteSubject class: ClockTimer
// ConcreteObserver class: DigitalClock
//         AnalogClock
//
// The ClockTimer object changes state by memeber function
// Tick(). This will trigger Update() function for both
// ConcreteObserver DigitalClock and AnalogClock. For demo,
// purpose, the observer objects will print out the current
// time in respond.
//
//=========================================================
#include "ClockTimer.h"
#include "DigitalClock.h"
#include "AnalogClock.h"

int main(void)
{
  ClockTimer timer;

  // Enforce a defined order of clock pointers to get a reproducible test output
  struct {
    AnalogClock analog;
    DigitalClock digital;
  } clocks;

  ClockObserver::aspectof ()->addObserver (&timer, &clocks.digital);
  ClockObserver::aspectof ()->addObserver (&timer, &clocks.analog);

  timer.Tick(); // subject state change and update all it's
                // observers which are digitalClock and
                // analogClock in this program
  timer.Tick(); // and again

  return 0;
}
