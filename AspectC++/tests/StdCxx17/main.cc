#include <iostream>
#include <cstdio>

using namespace std;

namespace N1::N2::N3 { // can nested namespaces be parsed?
  void deeply_nested_function() {
    cout << "  output of deeply nested function" << endl;
  }
}

using namespace N1::N2; // can nested namespaces be looked up?

namespace X1::X2 {}

namespace X1::X2::X3 {} // can an existing namespace be extended?

int main() {
  printf ("StdCxx17: Handling of new (C++17) language features\n");
  printf ("===================================================\n");
  N3::deeply_nested_function();
  printf ("===================================================\n");
}

aspect ExecTracer {
  advice execution ("% ...::%(...)" && !"% main(...)")  : around () {
    printf ("before %s\n", JoinPoint::signature ());
    tjp->proceed ();
    printf ("after %s\n", JoinPoint::signature ());
  }
};
