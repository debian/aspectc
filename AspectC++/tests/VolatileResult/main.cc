#include <stdio.h>

class Square {
public:
  volatile int x;
  volatile int get_x() const { return x; }
};

int main() {
  Square s;
  s.x = 42;
  if (--(s.x) == 41) {
    s.x++;
  }
  s.x = s.x + 4;
  s.x -= 8;
  s.x *= 6;
  if (s.get_x() != 228) {
    printf("error\n");
  }
}

void print_result(int* r) {
  printf("%i\n", *r);
}
void print_result(volatile int* r) {
  printf("%i (result type: volatile)\n", *r);
}

aspect ResultTesterAspect {
  advice get("Square") || call("Square") : around() {
    tjp->proceed();
    printf("%s: ", JoinPoint::signature());
    print_result(tjp->result());
    printf("%s: ", JoinPoint::signature());
    print_result((JoinPoint::Result*)tjp->result());
  }
};

