// See bug #792. Fine if it compiles without errors

class BX_CPU_C {
  void exception(unsigned vector, char error_code) const __attribute__((noreturn));
};
void BX_CPU_C::exception(unsigned vector, char error_code) const {
        while(1) {};
}

aspect FastBreakpoints {
    advice execution("void BX_CPU_C::exception(...)") : around () {
                unsigned I = *(tjp->arg<0>());
                while(1) {};
        };
};

int main () {
}
