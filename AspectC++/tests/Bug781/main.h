#ifndef __MAIN_H__
#define __MAIN_H__

#if   defined ( __ICCARM__ )
  #pragma system_include         /* treat file as system include file for MISRA check */
#elif defined (__clang__)
  #pragma clang system_header   /* treat file 
                                   as system include file */
#endif

#endif // __MAIN_H__
