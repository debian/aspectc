#ifndef __ac_infos_h__
#define __ac_infos_h__

#define AC_VERSION "2.3"
#define COMPANY_NAME "www.aspectc.org"
#define COPYRIGHT "Gnu Public License (GPL)"

#endif
