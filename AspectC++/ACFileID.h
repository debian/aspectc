// This file is part of the AspectC++ compiler 'ac++'.
// Copyright (C) 1999-2013  The 'ac++' developers (see aspectc.org)
//                                                                
// This program is free software;  you can redistribute it and/or 
// modify it under the terms of the GNU General Public License as 
// published by the Free Software Foundation; either version 2 of 
// the License, or (at your option) any later version.            
//                                                                
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
// GNU General Public License for more details.                   
//                                                                
// You should have received a copy of the GNU General Public      
// License along with this program; if not, write to the Free     
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
// MA  02111-1307  USA                                            

#ifndef __ACFileID_h__
#define __ACFileID_h__

#include <string>

#ifdef FRONTEND_PUMA
#include "Puma/Unit.h"
typedef const Puma::Unit *ACFileIDData;
#else
#include "version.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_18_1_3
typedef clang::OptionalFileEntryRef ACFileIDData;
#else
typedef const clang::FileEntry *ACFileIDData;
#endif
#endif

#include "version.h"

// A wrapper class that only allows to take the filename from a unit.
class ACFileID {

  ACFileIDData _entry;

public:

#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_18_1_3
  ACFileID (int arg = 0) : _entry(std::nullopt) { assert (arg==0); }
  ACFileID (clang::FileEntryRef entry) : _entry (entry) {}
#else
  ACFileID () : _entry(0) {}
#endif
  ACFileID (ACFileIDData entry) : _entry (entry) {}

  std::string name () const {
#ifdef FRONTEND_PUMA
    return _entry->name ();
#else
    return _entry->getName ().str ();
#endif
  }

  bool is_valid () const { return _entry != 0; }
  bool operator== (ACFileID rhs) const { return _entry == rhs._entry; }
  bool operator!= (ACFileID rhs) const { return !operator==(rhs); }
  bool operator< (ACFileID rhs) const { return _entry < rhs._entry; }
#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_18_1_3
  clang::FileEntryRef file_entry() const { return _entry.value (); }
#else
  ACFileIDData file_entry() const { return _entry; }
#endif

#ifdef FRONTEND_CLANG
  static ACFileID main_file (const clang::SourceManager &sm) {
#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_18_1_3
    return sm.getFileEntryRefForID (sm.getMainFileID ());
#else
    return sm.getFileEntryForID (sm.getMainFileID ());
#endif
  }

  static ACFileID from_location (clang::SourceLocation loc, const clang::SourceManager &sm) {
#if CLANG_VERSION_NUMBER >= VERSION_NUMBER_18_1_3
    return sm.getFileEntryRefForID(sm.getFileID(sm.getExpansionLoc (loc)));
#else
    return sm.getFileEntryForID(sm.getFileID(sm.getExpansionLoc (loc)));
#endif
  }

#endif
};

#endif // __ACFileID_h__
