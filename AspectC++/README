Installation
============

1. Make sure the following packages are installed:

   build-essential
   libxml2
   libxml2-dev
   zlib1g-dev

2. Get the Clang development package, which contains Clang and LLVM header
   files as well as the static LLVM and Clang libraries. There are three ways
   to achieve this:

     I On Debian and Ubuntu you can use the package libclang-3.5-dev or
       higher. The installation path is typically /usr/lib/llvm-3.5.

    II On other Linux systems you can download binary packages from llvm.org
       (http://releases.llvm.org/download.html). The tarballs can be extracted
       anywhere.

   III If there is no binary package for your system you can build Clang by
       yourself:
       a) Download LLVM and Clang sources from
          http://releases.llvm.org/download.html
       b) Follow http://clang.llvm.org/get_started.html to build LLVM and
          Clang.
       c) Install Clang with "make install" or by invoking the following
          command inside the Clang build directory (see
          http://llvm.org/docs/CMake.html for more information):
          cmake -DCMAKE_INSTALL_PREFIX=~/llvm-3.5 -P cmake_install.cmake

   In all cases: Locate llvm-config which is located inside <llvm-folder>/bin
                 and remember its path.

3. Please note that the Puma library is still needed for compiling
   AspectC++. In the Clang configuration we no longer need the Puma C++
   parser, but some other Puma functions are still referenced.
   Therefore, the "mini" configuration of the Puma library is sufficient. You
   can build the "mini" configuration by invoking the following command inside
   the Puma directory:

   make MINI=1

   The default target is "linux-release". You can specify the target with
   TARGET=<target>. For example:

   make MINI=1 TARGET=linux-debug

   Please note that the targets for Puma and AspectC++ (see step 4) should
   match.

   In this configuration Puma needs no aspect weaver for compilation.

4. Compile AspectC++ and set the make variable LLVMCONF to the path of
   llvm-config:

   make LLVMCONF=<llvm-config-path>

   In most cases you also want to add SHARED=1, -j<N>, and a TARGET. For
   example:

   make LLVMCONF=~/llvm-3.5/bin/llvm-config SHARED=1 TARGET=linux-debug -j4

5. Verify the AspectC++ installation by running the tests. For example:

   make testall LLVMCONF=~/llvm-3.5/bin/llvm-config TARGET=linux-debug

   Note: Missing includes may cause the tests to fail. In this case, ag++ can
         be used to generate it:

         ag++ --gen_config
         export PUMA_CONFIG="$(pwd)/puma.config"

         ag++ can be built by invoking "make" inside the Ag++ directory or can
         be downloaded from http://aspectc.org/Download.php.